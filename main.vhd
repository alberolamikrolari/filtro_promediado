----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:54:09 11/04/2013 
-- Design Name: 
-- Module Name:    main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Filtro_Promediado is

    generic (N : positive := 2;				--M = 2^N
			 M : positive := 4);
	
	port(		CLK 		: in  std_logic;
				Reset		: in  std_logic;
				Leds		: out std_logic_vector(7 downto 0);
				
				ADC_CLK		: out std_logic;
				ADC_MOSI	: out std_logic;
				ADC_MISO	: in  std_logic;
				ADC_CS		: out std_logic;
				
				DAC_CLK		: out std_logic;
				DAC_MOSI	: out std_logic;
				DAC_FS		: out std_logic);
			
end Filtro_Promediado;

architecture Behavioral of Filtro_Promediado is
	
	type Array_Conv is array (0 to M-1) of std_logic_vector(11+N downto 0);
	signal ADC_Conversion_Z		: Array_Conv;	
	signal ADC_Conversion		: std_logic_vector(11 downto 0);	
	signal ADC_CS_tmp			: std_logic;
	signal ADC_CS_Anterior		: std_logic;	
	signal DAC_Conversion		: std_logic_vector(11 downto 0);
	
	constant ADC_Prescaler		: std_logic_vector(4 downto 0) := "11000";
	constant ADC_Channel		: std_logic_vector(2 downto 0) := "001";		--100: potenciometro; 001: BNC
	constant DAC_Prescaler		: std_logic_vector(4 downto 0) := "11000";

	
	
	component ADC_Driver
	
		port(
		
			CLK 			: in  std_logic;
			Reset 			: in  std_logic;
			ADC_CLK			: out std_logic;
			ADC_MOSI		: out std_logic;
			ADC_MISO		: in  std_logic;
			ADC_CS			: out std_logic;
			ADC_Prescaler	: in  std_logic_vector(4 downto 0);
			ADC_Channel		: in  std_logic_vector(2 downto 0); 
			ADC_Conversion	: out std_logic_vector(11 downto 0));
	
	end component;
	
	component DAC_Driver
	
		port(
		
			CLK 			: in  std_logic;
			Reset 			: in  std_logic;
			DAC_CLK			: out std_logic;
			DAC_MOSI		: out std_logic;
			DAC_FS			: out std_logic;  
			DAC_Prescaler	: in  std_logic_vector(4 downto 0);
			DAC_Conversion	: in  std_logic_vector(11 downto 0));
	
	end component;

begin
	
	Inst_ADC_Driver: ADC_Driver 
	
		port map(
			CLK 			=> CLK,
			Reset 			=> Reset,
			ADC_CLK			=> ADC_CLK,
			ADC_MOSI		=> ADC_MOSI,
			ADC_MISO		=> ADC_MISO,
			ADC_CS			=> ADC_CS_tmp,
			ADC_Channel 	=> ADC_Channel,
			ADC_Prescaler	=> ADC_Prescaler,
			ADC_Conversion 	=> ADC_Conversion);
			
	
	Inst_DAC_Driver: DAC_Driver 
	
		port map(
			CLK 			=> CLK,
			Reset 			=> Reset,
			DAC_CLK			=> DAC_CLK,
			DAC_MOSI		=> DAC_MOSI,
			DAC_FS			=> DAC_FS,
			DAC_Prescaler	=> DAC_Prescaler,
			DAC_Conversion 	=> DAC_Conversion);

	
	Process_Promediado : process(CLK, Reset)
	
	variable Suma_Muestras_ADC : std_logic_vector(11 + N downto 0);
	
	begin
	
		if (Reset = '0') then
		
			DAC_Conversion 		<= (others => '0');
			ADC_Conversion_Z	<= (others => (others => '0'));
			ADC_CS_Anterior		<= '1';
		
		elsif (CLK'event and CLK = '1') then
			
			if (ADC_CS_tmp = '1' and ADC_CS_Anterior = '0') then
			
				for x in 0 to 11 loop								-- ADC_Conversion_Z(0) <= (others <= '0') & ADC_Conversion
				
					ADC_Conversion_Z(0)(x) <= ADC_Conversion(x);
					
				end loop;
				
				for x in 0 to (M-2) loop 							-- Desplazamos Z -> Z-1, Z-1 -> Z-2...	
																	
					ADC_Conversion_Z(x+1) <= ADC_Conversion_Z(x);		
																	
				end loop;													

				Suma_Muestras_ADC 	:= (others => '0');				-- Reseteamos Suma_Muestras_ADC y hacemos el promediado
				
				for x in 0 to (M-1) loop							
				
					Suma_Muestras_ADC := Suma_Muestras_ADC + ADC_Conversion_Z(x);
					
				end loop;	
				
				DAC_Conversion <= Suma_Muestras_ADC(11+N downto N);
					
			end if;
			
			ADC_CS_Anterior <= ADC_CS_tmp;
		
		end if;
	
	end process;
	
	Leds(7 downto 0) 	<= ADC_Conversion_Z(0)(11+N downto 4+N);	

	ADC_CS <= ADC_CS_tmp;

end Behavioral;

