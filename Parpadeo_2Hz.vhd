----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:46:41 12/23/2013 
-- Design Name: 
-- Module Name:    Parpadeo_2Hz - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Parpadeo_2Hz is

    Port ( CLK : in  STD_LOGIC;
           Reset : in  STD_LOGIC;
           Out_2Hz : out  STD_LOGIC);

end Parpadeo_2Hz;

architecture Behavioral of Parpadeo_2Hz is

	constant Prescaler		: std_logic_vector(23 downto 0) := x"BEBC20";
	signal   Contador		: std_logic_vector(23 downto 0);
	signal   Out_2Hz_tmp	: std_logic;  
           


begin

	Parpadeo : process(CLK, Reset)
	
	begin
	
		if(Reset = '0') then
		
			Contador 	<= (others => '0');
			Out_2Hz_tmp 	<= '0';
		
		elsif(CLK'event and CLK = '1') then
		
			if(Contador < Prescaler) then
				
				Contador <= Contador + 1;
			
			else
				
				Contador	<= (others => '0');
				Out_2Hz_tmp	<= not(Out_2Hz_tmp);
			
			end if;
		
		end if;
		
	end process;
	
	Out_2Hz <= Out_2Hz_tmp;
	
end Behavioral;