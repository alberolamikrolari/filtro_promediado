
-- VHDL Instantiation Created from source file ADC_Driver.vhd -- 22:42:55 12/23/2013
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT ADC_Driver
	PORT(
		CLK : IN std_logic;
		Reset : IN std_logic;
		ADC_Channel : IN std_logic_vector(2 downto 0);          
		ADC_Out : OUT std_logic_vector(11 downto 0)
		);
	END COMPONENT;

	Inst_ADC_Driver: ADC_Driver PORT MAP(
		CLK => ,
		Reset => ,
		ADC_Out => ,
		ADC_Channel => 
	);


