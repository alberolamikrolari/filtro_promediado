----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:20:00 12/23/2013 
-- Design Name: 
-- Module Name:    ADC_Driver - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ADC_Driver is

    Port (CLK 				: in  std_logic;
          Reset 			: in  std_logic;
		  ADC_CLK			: out std_logic;
		  ADC_MOSI			: out std_logic;
		  ADC_MISO			: in  std_logic;
		  ADC_CS			: out std_logic;
		  ADC_Prescaler		: in  std_logic_vector(4 downto 0);
		  ADC_Channel		: in  std_logic_vector(2 downto 0);
		  ADC_Conversion	: out std_logic_vector(11 downto 0));
		   
end ADC_Driver;

architecture Behavioral of ADC_Driver is

	signal		ADC_CLK_tmp		: std_logic;

	signal		ADC_Contador 	: std_logic_vector(4 downto 0);					-- Contador para el prescaler del DAC, a comparar con la  
																				-- constante "DAC_Prescaler"
	
	signal		ADC_Tx			: std_logic_vector(16 downto 0);				-- Dato registro desplazamiento
	
	signal		ADC_Rx_tmp		: std_logic_vector(16 downto 0);				-- Dato registro desplazamiento
	
	signal		ADC_Rx			: std_logic_vector(16 downto 0);				-- Registro actualizado al finalizar la conversion
																						
	signal		ADC_Estado		: natural range 0 to 16;						-- Estado de la maquina de estados del driver del DAC
																				-- (17 estados, del 0 al 16)
	



begin

	Process_ADC_Driver : process(CLK, Reset)									-- Maquina de estados en la que el contador es DESCENDENTE
																				-- El número del estado (de 17 a 0) se corresponde con 
	begin																		-- el bit transmitido / recibido
		
		if (Reset = '0') then
		
			ADC_Contador	<= (others => '0');
			ADC_Estado		<= 0;
			ADC_CLK_tmp 	<= '0';
			ADC_CS			<= '0';
			ADC_Tx			<= "1" & "01" & ADC_Channel & "10101010101"; 			-- Formato: 1 bit a '0' durante el tiempo de CS=1 +
																					-- + 2 bits "Don't Care" + 3 bits de seleccion de canal 
																					-- + 11 bits "Don't Care"
		elsif (CLK'event and CLK = '1') then							
				
			if (ADC_Contador = ADC_Prescaler) then									
			
				if (ADC_CLK_tmp = '1') then											-- Si va a haber un flanco descendente en DAC_CLK, 
																					-- avanzamos un estado en la maquina de estados.
					if (ADC_Estado = 0) then										-- Si estamos en el estado 0 (avanzariamos al 17, 
																					-- que no existe), reseteamos
						ADC_Estado		<= 16;										-- el contador de estados (DAC_Estado),
						ADC_CS			<= '1';										-- ponemos a '1' la salida CS (Chip Select del ADC), 
						ADC_Tx			<= "101" & ADC_Channel & "10101010101";		-- y cargamos el nuevo valor a convertir.
						ADC_Conversion 	<= ADC_Rx_tmp(11 downto 0);					-- Guardamos el valor de la conversion anterior
																					
					
					else														
					
						ADC_Estado 	<= ADC_Estado - 1;								-- Si no hemos llegado al estado 0, avanzamos un estado,
						ADC_CS		<= '0';											-- mantenemos FS a '0' (Frame Sync del DAC)
																	
					end if;
					
				else																-- Si va a haber un flanco ascendente en DAC_CLK, capturamos.
				
					ADC_Rx_tmp(ADC_Estado) <= ADC_MISO; 
				
				end if;	
					
				ADC_Contador <= (others => '0');
				ADC_CLK_tmp <= not(ADC_CLK_tmp);
					
			else
			
				ADC_Contador <= ADC_Contador + 1;
				
			end if;
			
			ADC_MOSI		<= ADC_Tx(ADC_Estado);
				
		end if;
		
	end process;

	ADC_CLK 		<= ADC_CLK_tmp;	
		

end Behavioral;

